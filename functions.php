<?php

const DATA_FILE = "data.txt";

function getPosts() {
    $lines = file(DATA_FILE);

    $posts = [];

    foreach ($lines as $line) {
        list($title, $body) = explode(";", $line);
        $posts[] = ["title" => urldecode($title), "body" => urldecode($body)];
    }

    return $posts;
}

function getPostByTitle($title) {
    $posts = getPosts();

    foreach ($posts as $post) {
        if ($post["title"] === $title) {
            return $post;
        }
    }

    return null;
}

function addPost($title, $body) {
    $line = urlencode($title) . ";" . urlencode($body) . PHP_EOL;
    file_put_contents(DATA_FILE, $line, FILE_APPEND);
}

function deletePost($title) {
    $posts = getPosts();
    $data = "";

    foreach ($posts as $post) {
        if ($post["title"] !== $title) {
            $data = $data . urlencode($post["title"]) . ";" . urlencode($post["body"]) . PHP_EOL;
        }
    }

    file_put_contents(DATA_FILE, $data);
}

function editPost($originalTitle, $title, $body) {
    $posts = getPosts();
    $data = "";

    foreach ($posts as $post) {
        if ($post["title"] === $originalTitle) {
            $post["title"] = $title;
            $post["body"] = $body;
        }

        $data = $data . urlencode($post["title"]) . ";" . urlencode($post["body"]) . PHP_EOL;
    }

    file_put_contents(DATA_FILE, $data);
}
<?php
    require_once("functions.php");
    $posts = array_reverse(getPosts());
?>


<!doctype html>
<html lang="en">
<head>
    <title>Blogi</title>
</head>
<body>
    <nav>
        <a href="add-post.php">Lisa postitus</a>
    </nav>
    <h1>Blogi</h1>

    <?php foreach ($posts as $each): ?>

        <div>
            <h2><?= $each["title"] ?></h2>

            <form action="delete-post.php" method="post">
                <input type="hidden" name="post-to-delete" value="<?= $each["title"] ?>">
                <input type="submit" name="delete" value="Kustuta" />
            </form>

            <a href="edit-post.php?title=<?= urlencode($each["title"]) ?>">Muuda</a>

            <p><?= $each["body"] ?></p>
            <hr/>
        </div>

    <?php endforeach; ?>

</body>
</html>
<?php
require_once("functions.php");

$errors = [];
$title = "";
$body = "";

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $title = isset($_POST["title"]) ? $_POST["title"] : "";
    $body = isset($_POST["body"]) ? $_POST["body"] : "";

    if (empty($title)) {
        $errors[] = "Sisesta pealkiri";
    }

    if (empty($body)) {
        $errors[] = "Sisesta sisu";
    }

    if (empty($errors)) {
        addPost($title, $body);
        header("Location: /");
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <title>Lisa postitus</title>
    <link rel="stylesheet" href="style.css"/>
</head>
<body>
<h1>Lisa postitus</h1>

<ul class="alert">
<?php foreach ($errors as $error): ?>
    <li><?= $error ?></li>
<?php endforeach; ?>
</ul>

<form action="add-post.php" method="post">
    <label for="title">Postituse pealkiri</label>
    <input id="title" type="text" name="title" placeholder="Postituse pealkiri" value="<?= $title ?>"/>
    <br/>
    <label for="body">Postituse sisu</label>
    <textarea id="body" name="body" placeholder="Postituse sisu"><?= $body ?></textarea>
    <br/>
    <input type="submit" value="Lisa">
</form>

</body>
</html>
<?php
require_once("functions.php");

$post = null;

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $title = $_POST["title"];
    $body = $_POST["body"];
    $originalTitle = $_POST["originalTitle"];
    editPost($originalTitle, $title, $body);
    header("Location: /");
} elseif ($_SERVER["REQUEST_METHOD"] === "GET") {
    if (isset($_GET["title"])) {
        $post = getPostByTitle($_GET["title"]);
    }
}


?>

<!doctype html>
<html lang="en">
<head>
    <title>Muuda postitust</title>
</head>
<body>
<h1>Postituse muutmine</h1>

<form action="edit-post.php" method="post">
    <label for="title">Postituse pealkiri</label>
    <input id="title"
           type="text"
           name="title"
           placeholder="Postituse pealkiri"
           value="<?= $post["title"] ?>"/>
    <br/>

    <label for="body">Postituse sisu</label>
    <textarea id="body" name="body" placeholder="Postituse sisu"><?= $post["body"] ?>
    </textarea>
    <br/>

    <input type="hidden" name="originalTitle" value="<?= $post["title"] ?>"/>
    <input type="submit" value="Salvesta">
</form>

</body>
</html>
